<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::resource('/operational', \App\Http\Controllers\OperationalController::class)->middleware('auth');

Route::middleware(['auth'])->namespace('Main')->group(function () {
    Route::get('/admin/home', 'HomeController@index')->name('admin.home.index');
    Route::resource('/admin/user', 'UserController');
    Route::resource('/admin/operational', 'OperationalController');
    Route::get('/admin/revenue', 'RevenueController@index')->name('admin.revenue.index');

    // Route::get('/admin/operational', 'Controller@index')->name('admin.operational');

});

@extends('layouts.app')

@section('content')
<div class="container" id="revenue-index">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5>Revenue</h5>
                </div>
                <div class="card-body">
                    <script>
                        let chartRevenue = @json($revenue)
                    </script>
                    <div class="row">
                        <div class="col-12">
                            <canvas id="myChart" width="5"></canvas>
                        </div>

                    </div>
                    <br>
                    <table class=" table">
                        <thead class="table-dark" style="background-color: #718096;">
                            <tr>
                                <th scope="col">Nomor</th>
                                <th scope="col">Date</th>
                                <th scope="col">Total Sell Price</th>
                                <th scope="col">Total Purch Price</th>
                                <th scope="col">Profit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($revenue as $key => $value)
                            <tr>
                                <th scope="row">{{ $value->id }}</th>
                                <td>{{ $value->date }}</td>
                                <td>{{ $value->total_selling_price }}</td>
                                <td>{{ $value->total_purchase_price }}</td>
                                <td>{{ $value->profit }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
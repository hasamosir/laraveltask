@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header text-white bg-primary">Edit Operational Cost</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('operational.update', $operational->id) }}">
                        @method('PUT')
                        @csrf
                        <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Item</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="item" value="{{$operational->item}}">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Price</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="price" value="{{$operational->price}}">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Purchase Date</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" name="purchase_date" value="{{$operational->purchase_date ?? date('Y-m-d')}}">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Notes</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="notes">{{$operational->notes}}</textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Edit</button>
                        <a href="{{route('operational.index')}}" type="button" class="btn btn-primary" style="float: right">Back</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
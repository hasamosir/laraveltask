@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5>Operational</h5>
                </div>
                <div class="card-body">
                    <div class="col d-flex align-items-center justify-content-between">
                        <a type="button" class="btn btn-sm btn-primary" href="{{ route('operational.create') }}">Add New</a>
                        <form class="form-inline my-2 my-lg-0">
                            <form method="GET" action="{{ route('operational.index') }}">
                                <input type="date" name="purchase_date_start" value="{{ $purchaseDateStart}}">
                                <input type="date" name="purchase_date_end" value="{{ $purchaseDateEnd}}">
                                <input type="submit" class="btn btn-sm btn-primary" value="filter">
                            </form>
                        </form>
                    </div>
                    <br>
                    <table class="table">
                        <thead class="table-dark" style="background-color: #718096;">
                            <tr>
                                <th scope="col">Nomor</th>
                                <th scope="col">Item</th>
                                <th scope="col">Price</th>
                                <th scope="col">Notes</th>
                                <th scope="col">Purchase Date</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($costs as $key => $cost)
                            <tr>
                                <th scope="row">{{ $key+1 }}</th>
                                <td>{{ $cost->item }}</td>
                                <td>{{ $cost->price }}</td>
                                <td>{{ $cost->notes }}</td>
                                <td>{{ $cost->purchase_date }}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-12">
                                            <a type="btn" class="btn btn-sm btn-info" href="{{route('operational.show', $cost->id)}}">Show</a>
                                            <a type="btn" class="btn btn-sm btn-primary" href="{{route('operational.edit', $cost->id)}}">Edit</a>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-12">
                                            <form method="POST" action="{{ route('operational.destroy', $cost->id) }}">
                                                @method('delete')
                                                @csrf
                                                <input type="submit" class="btn btn-sm btn-danger" value="delete">
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
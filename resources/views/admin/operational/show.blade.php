@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header text-white bg-primary">Show Operational Cost</div>
                <div class="card-body">
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Item</label>
                        <div class="col-sm-10">
                            <p>{{$operational->item}}</p>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Price</label>
                        <div class="col-sm-10">
                            <p>{{$operational->price}}</p>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Purchase Date</label>
                        <div class="col-sm-10">
                            <p>{{$operational->purchase_date}}</p>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Notes</label>
                        <div class="col-sm-10">
                            <p>{{$operational->notes}}</p>
                        </div>
                    </div>
                    <a href="{{route('operational.index')}}" type="button" class="btn btn-primary" style="float: right">Back</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
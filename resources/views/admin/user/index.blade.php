@extends('layouts.app')

@section('content')
<div class="container" id="user-index">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5>Users</h5>
                </div>
                <div class="card-body">
                    <div class="col d-flex align-items-center justify-content-between">
                        <a type="button" class="btn btn-sm btn-primary" href="{{ route('user.create') }}">Add New</a>
                    </div>
                    <br>
                    <table class=" table">
                        <thead class="table-dark" style="background-color: #718096;">
                            <tr>
                                <th scope="col">Nomor</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Access</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($access as $key => $value)
                            <tr>
                                <th scope="row">{{ $value->id }}</th>
                                <td>{{ $value->name }}</td>
                                <td>{{ $value->email }}</td>
                                <td>{{ $value->access->name ?? '-' }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Operational;
use Illuminate\Http\Request;

class OperationalController extends Controller
{
    private $operational;

    function __construct(Operational $_operationalCost)
    {
        $this->operational = $_operationalCost;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $purchaseDateStart = $request->purchase_date_start;
        $purchaseDateEnd = $request->purchase_date_end;

        if ($purchaseDateStart && $purchaseDateEnd) {
            $costs = $this->operational->whereBetween('purchase_date', [$purchaseDateStart, $purchaseDateEnd])->get();
        } else {
            $costs = $this->operational->all();
        }

        $purchaseDateStart = $purchaseDateStart ?? date('Y-m-d');
        $purchaseDateEnd = $purchaseDateEnd ?? date('Y-m-d');

        return view('admin.operational.index', compact('costs', 'purchaseDateStart', 'purchaseDateEnd'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.operational.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $operational = Operational::create([
            'item' => $request->item,
            'price' => $request->price,
            'purchase_date' => $request->purchase_date,
            'notes' => $request->notes,
        ]);


        if ($operational) {
            return redirect(route('operational.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Operational  $operational
     * @return \Illuminate\Http\Response
     */
    public function show(Operational $operational)
    {

        return view('admin.operational.show', compact('operational'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Operational  $operational
     * @return \Illuminate\Http\Response
     */
    public function edit(Operational $operational)
    {
        return view('admin.operational.edit', compact('operational'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Operational  $operational
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Operational $operational)
    {
        $operational->item = $request->item;
        $operational->price = $request->price;
        $operational->purchase_date = $request->purchase_date;
        $operational->notes = $request->notes;

        if ($operational->save()) {
            return redirect(route('operational.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Operational  $operational
     * @return \Illuminate\Http\Response
     */
    public function destroy(Operational $operational)
    {
        if ($operational->delete()) {
            return redirect(route('operational.index'));
        }
    }
}

<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Access;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accesses = $this->checkAccesses();

        $access = User::all();
        return view('admin.user.index', compact('access', 'accesses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $accesses = $this->checkAccesses();
        $accessAll = Access::all();
        return view('admin.user.add', compact('accessAll'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $accesses = $this->checkAccesses();
        $isExist = User::where('email', $request->email)->first();
        if ($isExist) {
            echo 'email already exists';
        } else {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = password_hash("admin123", PASSWORD_BCRYPT);
            $user->access_id = $request->access_id;

            if ($user->save()) {
                return redirect(route('user.index'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->checkAccesses();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->checkAccesses();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->checkAccesses();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->checkAccesses();
    }

    private function checkAccesses()
    {
        $userAccesses = auth()->user()->access->name ?? null;
        if ($userAccesses !== 'owner') {
            abort(404);
        }
        return true;
    }
}
